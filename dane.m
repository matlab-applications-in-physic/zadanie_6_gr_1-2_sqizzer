%Made by Lukasz Giemza
%03.12.2019
%Final version

%I am not going to comment every line, because most lines are almost the
%same execpt variables
clear
addpath('C:\Users\student\Desktop\matlabG\jsonlab-1.9.8'); %adding the path where jsonLab is located


y = urlread('http://wttr.in/opole?format=j1'); %loading url sites
x = urlread('https://danepubliczne.imgw.pl/api/data/synop/format/json/station/opole'); 
y1 = urlread('http://wttr.in/raciborz?format=j1');
x1 = urlread('https://danepubliczne.imgw.pl/api/data/synop/format/json/station/raciborz');
y2 = urlread('http://wttr.in/katowice?format=j1');
x2 = urlread('https://danepubliczne.imgw.pl/api/data/synop/format/json/station/katowice');
stacja = loadjson(x);  %making jsonformat able to use in matlab
temp = stacja.temperatura;  %assign temperature
cis = stacja.cisnienie;       %assign pressure
pred = stacja.predkosc_wiatru; %assign wind speed
wilg = stacja.wilgotnosc_wzgledna; %assign humidity
czas = stacja.godzina_pomiaru;  %assign time
wilg1 = str2num(wilg); %converting values from strings to numbers
czas1 = str2num(czas);
temp1 = str2num(temp);
cis1 = str2num(cis);
pred1 = str2num(pred);

%same as before but date is from another station
stacjax1 = loadjson(x1);  
tempx1 = stacjax1.temperatura;
cisx1 = stacjax1.cisnienie;
predx1 = stacjax1.predkosc_wiatru;
wilgx1 = stacjax1.wilgotnosc_wzgledna;
czasx1 = stacjax1.godzina_pomiaru;
wilg1x1 = str2num(wilgx1);
czas1x1 = str2num(czasx1);
temp1x1 = str2num(tempx1);
cis1x1 = str2num(cisx1);
pred1x1 = str2num(predx1);

%same as before but date is from another station
stacjax2 = loadjson(x2);  
tempx2 = stacjax2.temperatura;
cisx2 = stacjax2.cisnienie;
predx2 = stacjax2.predkosc_wiatru;
wilgx2 = stacjax2.wilgotnosc_wzgledna;
czasx2 = stacjax2.godzina_pomiaru;
wilg1x2 = str2num(wilgx2);
czas1x2 = str2num(czasx2);
temp1x2 = str2num(tempx2);
cis1x2 = str2num(cisx2);
pred1x2 = str2num(predx2);





stacja2 = loadjson(y);

s = cell2mat((stacja2.current_condition)); %going deeper into the structure "s"
wiatr = s.windspeedMiles; %assign wind speed in miles per hour
tem = s.temp_F; %assign temperature in Farenhaits
wil = s.humidity; %assign humidity
wiatr1 = str2num(s.windspeedMiles); %convering values from strings to numbers
tem1 = str2num(s.temp_F);
tem1C = (tem1-32)/1.8;  %converting temperature form Farenhaits to Celcius
wil1 = str2num(s.humidity);
w = cell2mat((stacja2.weather));  %assign date and time
wd = w.date;
wh = w.hourly;
wh2 = cell2mat(wh);
wh3 = wh2.time;


stacja2y1 = loadjson(y1);
%same as before but date is from another station
sy1 = cell2mat((stacja2y1.current_condition));
wiatry1 = sy1.windspeedMiles; 
temy1 = sy1.temp_F; 
wily1 = sy1.humidity; 
wiatr1y1 = str2num(sy1.windspeedMiles);
tem1y1 = str2num(sy1.temp_F);
tem1Cy1 = (tem1y1-32)/1.8;
wil1y1 = str2num(sy1.humidity);
wy1 = cell2mat((stacja2y1.weather));
wdy1 = wy1.date;
why1 = wy1.hourly;
wh2y1 = cell2mat(why1);
wh3y1 = wh2y1.time;

stacja2y2 = loadjson(y2);
%same as before but date is from another station
sy2 = cell2mat((stacja2y2.current_condition));
wiatry2 = sy2.windspeedMiles; 
temy2 = sy2.temp_F; 
wily2 = sy2.humidity; 
wiatr1y2 = str2num(sy2.windspeedMiles);
tem1y2 = str2num(sy2.temp_F);
tem1Cy2 = (tem1y2-32)/1.8;
wil1y2 = str2num(sy2.humidity);
wy2 = cell2mat((stacja2y2.weather));
wdy2 = wy2.date;
why2 = wy2.hourly;
wh2y2 = cell2mat(why2);
wh3y2 = wh2y2.time;
DatNumber = datenum(wd);



%calculating the Fellslike Temperature

if tem1 <= 50 & wiatr1 >= 3
    vFeelsLike = 35.74 + (0.6215*tem1) - 35.75*(wiatr1^0.16) + ((0.4275*tem1)*(wiatr1^0.16));
else
    vFeelsLike = tem1;
end
if vFeelsLike == tem1 & tem1 >= 80;
    vFeelsLike = 0.5 * (tem1 + 61.0 + ((tem1-68.0)*1.2) + (wil1*0.094));
end
 
if vFeelsLike >= 80
    vFeelsLike = -42.379 + 2.04901523*tem1 + 10.14333127*wil1 - .22475541*tem1*wil1 - .00683783*tem1*tem1 - .05481717*wil1*wil1 + .00122874*tem1*tem1*wil1 + .00085282*tem1*wil1*wil1 - .00000199*tem1*tem1*wil1*wil1;
end
if wil1 < 13 & tem1 >= 80 & tem1 <= 112;
    vFeelsLike = vFeelsLike - ((13-wil1)/4)*sqrt((17-abs(tem1-95.))/17);
end
if wil1 > 85 & tem1 >= 80 & tem1 <= 87;
    vFeelsLike = vFeelsLike + ((wil1-85)/10) * ((87-tem1)/5);
end
vFeelsLikeCelcius = (vFeelsLike-32)/1.8; 




if tem1y1 <= 50 & wiatr1y1 >= 3;
    vFeelsLikey1 = 35.74 + (0.6215*tem1y1) - 35.75*(wiatr1y1^0.16) + ((0.4275*tem1y1)*(wiatr1y1^0.16));
else
    vFeelsLikey1 = tem1y1;
end
if vFeelsLikey1 == tem1y1 & tem1y1 >= 80;
    vFeelsLikey1 = 0.5 * (tem1y1 + 61.0 + ((tem1y1-68.0)*1.2) + (wil1y1*0.094));
end
 
if vFeelsLikey1 >= 80
    vFeelsLikey1 = -42.379 + 2.04901523*tem1y1 + 10.14333127*wil1y1 - .22475541*tem1y1*wil1y1 - .00683783*tem1y1*tem1y1 - .05481717*wil1y1*wil1y1 + .00122874*tem1y1*tem1y1*wil1y1 + .00085282*tem1y1*wil1y1*wil1y1 - .00000199*tem1y1*tem1y1*wil1y1*wil1y1;
end
if wil1y1 < 13 & tem1y1 >= 80 & tem1y1 <= 112
    vFeelsLikey1 = vFeelsLikey1 - ((13-wil1y1)/4)*sqrt((17-abs(tem1y1-95.))/17);
end
if wil1y1 > 85 & tem1y1 >= 80 & tem1y1 <= 87
    vFeelsLikey1 = vFeelsLikey1 + ((wil1y1-85)/10) * ((87-tem1y1)/5);
end
vFeelsLikeCelciusy1 = (vFeelsLikey1-32)/1.8;              

%Taken from  https://gist.github.com/jfcarr/e68593c92c878257550d?fbclid=IwAR1We5QdcSEbG6ohCisCfCa6Syi2aUpyUCBe_jROfZx7z92xnLj5WwCXp1s
%It is a pattern to calculate FeelsLike temperature 


if tem1y2 <= 50 & wiatr1y2 >= 3
    vFeelsLikey2 = 35.74 + (0.6215*tem1y2) - 35.75*(wiatr1y2^0.16) + ((0.4275*tem1y2)*(wiatr1y2^0.16));
else
    vFeelsLikey2 = tem1y2;
end
if vFeelsLikey2 == tem1y2 & tem1y2 >= 80;
    vFeelsLikey2 = 0.5 * (tem1y2 + 61.0 + ((tem1y2-68.0)*1.2) + (wil1y2*0.094));
end
 
if vFeelsLikey2 >= 80
    vFeelsLikey2 = -42.379 + 2.04901523*tem1y2 + 10.14333127*wil1y2 - .22475541*tem1y2*wil1y2 - .00683783*tem1y2*tem1y2 - .05481717*wil1y2*wil1y2 + .00122874*tem1y2*tem1y2*wil1y2 + .00085282*tem1y2*wil1y2*wil1y2 - .00000199*tem1y2*tem1y2*wil1y2*wil1y2;
end
if wil1y2 < 13 & tem1y2 >= 80 & tem1y2 <= 112
    vFeelsLikey2 = vFeelsLikey2 - ((13-wil1y2)/4)*sqrt((17-abs(tem1y2-95.))/17);
end
if wil1y2 > 85 & tem1y2 >= 80 & tem1y2 <= 87;
    vFeelsLikey2 = vFeelsLikey2 + ((wil1y1-85)/10) * ((87-tem1y1)/5);
end
vFeelsLikeCelciusy2 = (vFeelsLikey2-32)/1.8; 



%adding all the varaibles to 1 matrix

temperatury = [temp1,tem1C,temp1x1,tem1Cy1,temp1x2,tem1Cy2]; %matrix created to find max and min Temperature
maxT = max(temperatury); %searching for the maximum value  (max function)  
minT = min(temperatury); %searching for the minimum value   (min fuction)
TemperaturyOdczuwalne = [vFeelsLikeCelcius,vFeelsLikeCelciusy1,vFeelsLikeCelciusy2];  %matrix created to find max and min FellsLike Temperature
maxTodcz = max(TemperaturyOdczuwalne);
minTodcz = min(TemperaturyOdczuwalne);
cisnienie = [cis1,cis1x1,cis1x2];  %matrix created to find max and min pressure
MaxCis = max(cisnienie);
MinCis = min(cisnienie);
wilgotnosc = [wilg1,wilg1x1,wilg1x2]; %matrix created to find max and min humidity
MaxWilgotnosc = max(wilgotnosc);
MinWilgotnosc = min(wilgotnosc);
wilgotnoscWzg = [wil1,wil1y1,wil1y2]; %matrix created to find max and min relative humidity
MaxWilogtnoscWzgledna = max(wilgotnoscWzg);
MinWilogtnoscWzgledna = min(wilgotnoscWzg);

newdata = [pred1,wilg1,temp1,czas1,cis1,wiatr1,tem1,wil1,vFeelsLikeCelcius,pred1x1,wilg1x1,temp1x1,czas1x1,cis1x1,wiatr1y1,tem1y1,wil1y1,vFeelsLikeCelciusy1,pred1x2,wilg1x2,temp1x2,czas1x2,cis1x2,wiatr1y2,tem1y2,wil1y2,vFeelsLikeCelciusy2,maxT,minT,maxTodcz,minTodcz,MaxWilogtnoscWzgledna,MinWilogtnoscWzgledna,MinWilgotnosc,MaxWilgotnosc,MaxCis,MinCis];
%creating a matrix from all the variables
fileID = fopen('tekst1.txt','w'); %writing values to txt file 
fprintf(fileID, 'DLA STACJI W OPOLU: \n');
fprintf(fileID,'Predkosc wiatru wyraona w kilometrach na godzine odczytana z stacji w opolu (danepubliczne) wynosi  %f\n', pred1);
fprintf(fileID,'Wilogotno powietrza wyraona w procentach odczytana z stacji w opolu (danepubliczne)  wynosi %f\n', wilg1);
fprintf(fileID,'Temperatura wyraona w stopniach Celcjusza odczytana z stacji w opolu (danepubliczne)  wynosi %f\n', temp1);
fprintf(fileID,'Czas wyraony w godzinie odczytany z stacji w opolu (danepubliczne) wynosi %f\n', czas1);
fprintf(fileID,'Cisnienie wyraone w hPa odczytana z stacji w opolu (danepubliczne) wynosi %f\n', cis1);


fprintf(fileID,'Predkosc wiatru wyraona w milach na godzine odczytana z stacji w opolu (wttr) wynosi  %f\n', wiatr1);
fprintf(fileID,'Temperatura powietrza wyraona w Celcjuszach odczytana z stacji w opolu (wttr)  %f\n', tem1C);
fprintf(fileID,'Wilogtnosc wyrazona w procentach odczynana z stacji w opolu (wttr) wynosi  %f\n', wil1);
fprintf(fileID,'Temperatura odczuwalna wyraona w Celcjuszach odczytana z stacji w opolu (wttr)  %f\n', vFeelsLikeCelcius);
fprintf(fileID,'\n');

fprintf(fileID, 'DLA STACJI W RACIBORZU: \n');
fprintf(fileID,'Predkosc wiatru wyraona w kilometrach na godzine odczytana z stacji w raciborzu (danepubliczne) wynosi  %f\n', pred1x1);
fprintf(fileID,'Wilogotno powietrza wyraona w procentach odczytana z stacji w raciborzu (danepubliczne)  wynosi %f\n', wilg1x1);
fprintf(fileID,'Temperatura wyraona w stopniach Celcjusza odczytana z stacji w raciborzu(danepubliczne)  wynosi %f\n', temp1x1);
fprintf(fileID,'Czas wyraony w godzinie odczytany z stacji w raciborzu (danepubliczne) wynosi %f\n', czas1x1);
fprintf(fileID,'Cisnienie wyraone w hPa odczytana z stacji w raciborzu (danepubliczne) wynosi %f\n', cis1x1);


fprintf(fileID,'Predkosc wiatru wyraona w milach na godzine odczytana z stacji w raciborzu (wttr) wynosi  %f\n', wiatr1y1);
fprintf(fileID,'Temperatura powietrza wyraona w Celcuszach odczytana z stacji w raciborzu(wttr)  %f\n', tem1Cy1);
fprintf(fileID,'Wilogtnosc wyrazona w procentach odczynana z stacji w opolu (wttr) raciborzu  %f\n', wil1y1);
fprintf(fileID,'Temperatura odczuwalna wyraona w Celcjuszach odczytana z stacji w raciborzu (wttr)  %f\n', vFeelsLikeCelciusy1);
fprintf(fileID,'\n');

fprintf(fileID, 'DLA STACJI W KATOWICACH: \n');
fprintf(fileID,'Predkosc wiatru wyraona w kilometrach na godzine odczytana z stacji w katowicach (danepubliczne) wynosi  %f\n', pred1x2);
fprintf(fileID,'Wilogotno powietrza wyraona w procentach odczytana z stacji w katowicach (danepubliczne)  wynosi %f\n', wilg1x2);
fprintf(fileID,'Temperatura wyraona w stopniach Celcjusza odczytana z stacji w katowicach(danepubliczne)  wynosi %f\n', temp1x2);
fprintf(fileID,'Czas wyraony w godzinie odczytany z stacji w katowicach (danepubliczne) wynosi %f\n', czas1x2);
fprintf(fileID,'Cisnienie wyraone w hPa odczytana z stacji w katowicach (danepubliczne) wynosi %f\n', cis1x2);


fprintf(fileID,'Predkosc wiatru wyraona w milach na godzine odczytana z stacji w katowicach (wttr) wynosi  %f\n', wiatr1y2);
fprintf(fileID,'Temperatura powietrza wyraona w Celcujszach odczytana z stacji w katowicach(wttr)  %f\n', tem1Cy2);
fprintf(fileID,'Wilogtnosc wyrazona w procentach odczynana z stacji w opolu (wttr) katowicach  %f\n', wil1y2);
fprintf(fileID,'Temperatura odczuwalna wyraona w Celcjuszach odczytana z stacji w katowicach (wttr)  %f\n', vFeelsLikeCelciusy2);

fprintf(fileID,'\n');
fprintf(fileID,'\n');

fprintf(fileID,'Temperatura maksymalna wyrazona w Celcjuszach wynosi  %f\n', maxT);
fprintf(fileID,'Temperatura minimalna wyrazona w Celcjuszach wynosi  %f\n', minT);
fprintf(fileID,'Odczuwalna Temperatura maksymalna wyrazona w Celcjuszach wynosi  %f\n', maxTodcz);
fprintf(fileID,'Odczuwalna Temperatura minimalna wyrazona w Celcjuszach wynosi  %f\n', minTodcz);
fprintf(fileID,'Wzgledna wilgotnosc maksymalna wyrazona w procentach wynosi  %f\n',MaxWilogtnoscWzgledna);
fprintf(fileID,'Wzgledna wilgotnosc minimalna wyrazona w procentach wynosi  %f\n', MinWilogtnoscWzgledna);
fprintf(fileID,'wilgotnosc maksymalna wyrazona w procentach wynosi  %f\n', MaxWilgotnosc);
fprintf(fileID,'wilgotnosc minimalna wyrazona w procentach  %f\n', MinWilgotnosc);
fprintf(fileID,'Cisnienie maksymalne wyrazone w Hpa wynosi  %f\n', MaxCis);
fprintf(fileID,'Cisnienie minimalne wyrazone w Hpa wynosi  %f\n', MinCis);





csvwrite('liczby1.csv',newdata) %writing variables to csv file
disp('Dane znajduj si w plikach "liczby1.csv" oraz "tekst1.txt". Zostay przeprowadzone dnia:')
disp(wd)



